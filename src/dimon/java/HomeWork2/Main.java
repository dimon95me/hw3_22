package dimon.java.HomeWork2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner in = new Scanner(System.in);
        System.out.println("Enter strings count:");
        int n = in.nextInt();
        String[] strings = new String[n];

        for(int i=0; i<n; i++){
            System.out.println("Enter new string:");
            strings[i]=in.next();
        }


        int allStringLength = 0;
        for (String string : strings) {
            allStringLength+=string.length();
        }
        int avg =Math.round(allStringLength/n);
        System.out.println(avg);
        for (String item : strings) {
            if(item.length()<avg){
                System.out.println(item);
            }
        }

    }
}
